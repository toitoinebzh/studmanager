# Stud Manager

This software is designed to help you to take care of your horses.


It has the following features :

  * Health Records
  * Movement Records
  * Address Book
  * Reminders
  * Cross platform software

![Demo](demo.gif)

Enjoy and have fun ;-)

## Licence

This software is distributed under GPL v3 or later.

_____________________________

[Changelog](https://framagit.org/anto1ne/studmanager/-/releases)  
[Authors](AUTHORS.md)  
[PPA](https://launchpad.net/~antoine-/+archive/ubuntu/studmanager)