# Maintainer

 * Antoine : since 2018

# Main Authors

 * Antoine : created this software in 2018

# Code Contributors

 * moths-art

# Contributors

 * [Ubuntu-fr.org](https://ubuntu-fr.org/) : bipede, kikito, Pimprelune, moths-art, grim7reaper, Roschan
 * [Developpez.com](https://www.developpez.net) : VinsS
 * [IRC channel #pyqt](irc://irc.freenode.org/pyqt) : Avaris, altendky

 
# Graphics
 
 * Thanks to [openclipart.org](https://openclipart.org/) for providing severals icons used in this software. 
      * horse.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/6163/chess-horse
    
     * horse_shoe.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/215358/horse-shoe
    
     * syringe.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/205886/syringe
    
     * movements.ico
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/27557/iso-truck-3
         
 * Thanks to [forkaweso.me](https://forkaweso.me/Fork-Awesome/icons/) for providing severals icons used in this software. 
     * medkit.svg
         * Licence : SIL Open Font License (OFL)
         * https://forkaweso.me/Fork-Awesome/icon/medkit/
     * bell.svg
         * Licence : SIL Open Font License (OFL)
         * https://forkaweso.me/Fork-Awesome/icon/bell/
     * user-o.svg
         * Licence : SIL Open Font License (OFL)
         * https://forkaweso.me/Fork-Awesome/icon/user-o/
