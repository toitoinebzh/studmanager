#!/bin/bash
# script to generate the translation files

cd ..

locale="locale/fr_FR"
liste_fich_py=$(find $4 -name '*.py')
liste_fich_ui=$(find $4 -name '*.ui')
liste_fich="$liste_fich_py $liste_fich_ui"
echo "=== Generate .ts file ==="

for fich in $liste_fich; do
	if [ -s $fich ]; then
		echo $fich
	fi
done

pylupdate5 $liste_fich -ts $locale.ts

echo "=== Generate .qm file ==="

read -p "Translate the .ts file with Qt Linguist then press Enter to continue"

lrelease $locale.ts

echo "=== Update .qm file ==="

#pylupdate5 $liste_fich -ts -noobsolete -verbose $locale.ts
pylupdate5 $liste_fich -ts $locale.ts
echo "Done"
