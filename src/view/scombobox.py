#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 12:15:38 2020

@author: antoine
"""
from PyQt5.QtWidgets import QComboBox
from model.list_model import ListModel


class SComboBoxObj(QComboBox):
    def __init__(self, parent, studdb=None, list_obj=None, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        #self.list_obj = list_obj
        if (self.dtb is not None) and (list_obj is not None):
            self.update(self.dtb, list_obj, obj)

    def update(self, studdb, list_obj, obj=None):
        self.dtb = studdb
        #self.list_obj = list_obj
        #self.list_model = ListModel(self.dtb, list_obj)
        self.setModel(ListModel(self.dtb, list_obj))
        if obj is not None:
            self.set_obj(obj)

    def get_obj(self):
        return self.model().get_items()[self.currentIndex()]

    def set_obj(self, obj):
        if (obj is not None) and (obj in self.model().items()):
            index = self.model().get_items().index(obj)
            self.setCurrentIndex(index)


if __name__ == '__main__':
    import sys
    from studdb import StudDb
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    list_obj = dtb.table_contact.get_contacts
    obj = list_obj()[1]
    widget = SComboBoxObj(app, dtb, list_obj, obj)
    #widget = SComboBoxObj(app)
    widget.show()
    sys.exit(app.exec_())
