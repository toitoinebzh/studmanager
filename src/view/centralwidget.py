#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 19:56:29 2020

@author: antoine
"""

import sys
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtGui import QIcon

from controller.horse_ui import HorseUI
from controller.contact_ui import ContactUI
from controller.table_reminder_view import TableReminderView


class CentraWidget(QTabWidget):
    "widget to describe a horse"

    def __init__(self, studdb, horse=None):
        QTabWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        # Add tabs
        self.addTab(HorseUI(self.dtb, self.horse), self.tr("My Horses"))
        self.addTab(ContactUI(self.dtb), self.tr("Contacts"))

        self.addTab(TableReminderView(self.dtb), self.tr("Reminders"))

        # tab icon
        #self.setTabIcon(0, QIcon.fromTheme("go-home"))
        self.setTabIcon(0, QIcon("./icons/horse.svg"))
        self.setTabIcon(1, QIcon.fromTheme("x-office-address-book"))
        self.setTabIcon(2, QIcon.fromTheme("task-due"))
# self.main_tabs.setTabIcon(4, QIcon.fromTheme("accessories-dictionary"))
# # reports


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb
    from studdb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    #horse = dtb.table_horse.get_horses()[1]
    horse = Horse(dtb, 1)
    widget = CentraWidget(dtb, None)
    widget.show()
    sys.exit(app.exec_())
