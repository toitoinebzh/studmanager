#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    classes to describes Horses :
        Horse, Breed, Color, Sex
        MovementRecord, HealthRecord
"""

from datetime import date

from model.contactdb import Contact


class MovementRecord():
    "class to represent a movment record"

    def __init__(self, dtb, id_mr):
        self.dtb_id = id_mr
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_date(self):
        "return movement date"
        self.dtb.cur.execute(
            '''select TMovementRecords.date from TMovementRecords
                         where TMovementRecords.id= ? ''', (self.dtb_id, ))
        mr_date = self.dtb.cur.fetchone()[0]
        mr_date_split = mr_date.split('-')
        return date(
            int(mr_date_split[0]), int(mr_date_split[1]), int(
                mr_date_split[2]))

    def get_horse(self):
        "return horse"
        self.dtb.cur.execute(
            '''select TMovementRecords.id_horse from TMovementRecords
                            where TMovementRecords.id= ? ''', (self.dtb_id, ))
        id_horse = self.dtb.cur.fetchone()[0]
        return Horse(self.dtb, id_horse)

    def get_comments(self):
        "return comment"
        self.dtb.cur.execute(
            '''select TMovementRecords.comments from TMovementRecords
                            where TMovementRecords.id= ? ''', (self.dtb_id, ))
        comments = self.dtb.cur.fetchone()[0]
        return comments

    def __repr__(self):
        return "==\n{}\n{} \n{}\n==".format(self.get_date(),
                                            self.get_horse().get_name(),
                                            self.get_comments())


class HealthRecord():
    "class to represent a health record"

    def __init__(self, dtb, id_hr):
        self.dtb_id = id_hr
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_date(self):
        "return date"
        self.dtb.cur.execute('''select THealthRecords.date from THealthRecords
                         where THealthRecords.id= ? ''', (self.dtb_id, ))
        hr_date = self.dtb.cur.fetchone()[0]
        hr_date_split = hr_date.split('-')
        return date(
            int(hr_date_split[0]), int(hr_date_split[1]), int(
                hr_date_split[2]))

    def get_contact(self):
        "return contact"
        self.dtb.cur.execute(
            '''select THealthRecords.id_contact from THealthRecords
                            where THealthRecords.id= ? ''', (self.dtb_id, ))
        id_contact = self.dtb.cur.fetchone()[0]
        return Contact(self.dtb, id_contact)

    def get_horse(self):
        "return horse"
        self.dtb.cur.execute(
            '''select THealthRecords.id_horse from THealthRecords
                            where THealthRecords.id= ? ''', (self.dtb_id, ))
        id_horse = self.dtb.cur.fetchone()[0]
        return Horse(self.dtb, id_horse)

    def get_comments(self):
        "return comment"
        self.dtb.cur.execute(
            '''select THealthRecords.comments from THealthRecords
                            where THealthRecords.id= ? ''', (self.dtb_id, ))
        comments = self.dtb.cur.fetchone()[0]
        return comments

    def __repr__(self):
        return "==\n{}\n{}\n{} \n{}\n==".format(
            self.get_date(),
            self.get_horse().get_name(),
            self.get_contact().get_full_name(),
            self.get_comments())


class Sex():
    "class to represent sex"

    def __init__(self, dtb, id_sex):
        self.dtb_id = id_sex
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_name(self):
        "return sex name"
        self.dtb.cur.execute('''select TSex.name from TSex
                            where TSex.id= ? ''', (self.dtb_id, ))
        name = self.dtb.cur.fetchone()[0]
        return name

    def __repr__(self):
        return self.get_name()


class Color():
    "class to represent color"

    def __init__(self, dtb, id_color):
        self.dtb_id = id_color
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_name(self):
        "return color name"
        self.dtb.cur.execute('''select TColor.name from TColor
                            where TColor.id= ? ''', (self.dtb_id, ))
        name = self.dtb.cur.fetchone()[0]
        return name

    def __repr__(self):
        return self.get_name()


class Breed():
    "class to represent breed"

    def __init__(self, dtb, id_breed):
        self.dtb_id = id_breed
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_name(self):
        "return breed name"
        self.dtb.cur.execute('''select TBreed.name from TBreed
                            where TBreed.id= ? ''', (self.dtb_id, ))
        name = self.dtb.cur.fetchone()[0]
        return name

    def __repr__(self):
        return self.get_name()


class Horse():
    "class to represent horse"

    def __init__(self, dtb, id_horse):
        self.dtb_id = id_horse
        self.dtb = dtb

    def __eq__(self, other):
        return (self.dtb_id == other.dtb_id) and (self.dtb == other.dtb)

    def get_name(self):
        "return horse name"
        self.dtb.cur.execute('''select THorse.name from THorse
                         where THorse.id= ? ''', (self.dtb_id, ))
        name = self.dtb.cur.fetchone()[0]
        return name

    def get_breed(self):
        "return horse breed"
        self.dtb.cur.execute('''select TBreed.id from TBreed
                         left join THorse on THorse.id_breed=TBreed.id
                         where THorse.id= ? ''', (self.dtb_id, ))
        id_breed = self.dtb.cur.fetchone()[0]
        return Breed(self.dtb, id_breed)

    def get_color(self):
        "return horse color"
        self.dtb.cur.execute('''select TColor.id from TColor
                         left join THorse on THorse.id_color=TColor.id
                         where THorse.id= ? ''', (self.dtb_id, ))
        id_color = self.dtb.cur.fetchone()[0]
        return Color(self.dtb, id_color)

    def get_sex(self):
        "return horse sex"
        self.dtb.cur.execute('''select TSex.id from TSex
                         left join THorse on THorse.id_sex=TSex.id
                         where THorse.id= ? ''', (self.dtb_id, ))
        id_sex = self.dtb.cur.fetchone()[0]
        return Sex(self.dtb, id_sex)

    def get_birthdate(self):
        "return birthdate"
        self.dtb.cur.execute('''select THorse.birthdate from THorse
                         where THorse.id= ? ''', (self.dtb_id, ))
        birthdate = self.dtb.cur.fetchone()[0]
        dob = birthdate.split('-')
        return date(int(dob[0]), int(dob[1]), int(dob[2]))

    def get_health_records(self):
        "return all health record of this horse"
        self.dtb.cur.execute('''select THealthRecords.id from THealthRecords
                         left join THorse on THorse.id=THealthRecords.id_horse
                         where THorse.id= ? ORDER BY THealthRecords.date DESC''',
                             (self.dtb_id, ))
        list_id_hr = self.dtb.cur.fetchall()
        return [HealthRecord(self.dtb, item[0]) for item in list_id_hr]

    def get_movement_records(self):
        "return all movements records of this horse"
        self.dtb.cur.execute(
            '''select TMovementRecords.id from TMovementRecords
                         left join THorse on THorse.id=TMovementRecords.id_horse
                         where THorse.id= ? ORDER BY TMovementRecords.date DESC, TMovementRecords.id DESC''',
            (self.dtb_id, ))
        list_id_mr = self.dtb.cur.fetchall()
        return [MovementRecord(self.dtb, item[0]) for item in list_id_mr]

    def is_in(self):
        "return bool to know if horse is in the stud"
        movement_nb = len(self.get_movement_records())
        if movement_nb % 2 == 0:
            return False
        else:
            return True

    def __repr__(self):
        return "({}, {}, {})".format(self.get_name(),
                                     self.get_sex().get_name(),
                                     self.get_birthdate())
