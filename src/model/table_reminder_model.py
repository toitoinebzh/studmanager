#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 14:09:40 2020

@author: antoine
"""

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# https://www.learnpyqt.com/courses/model-views/modelview-architecture/

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt


class TableReminderModel(QAbstractTableModel):

    def __init__(self, studb, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.dtb = studb
        self.items = self.dtb.table_reminders.get_reminders

        self.header = [
            self.tr("Date"),
            self.tr("Horse"),
            self.tr("Comments"),
            self.tr("Done")]

    def get_items(self):
        return self.items()

    def columnCount(self, parent=QModelIndex()):
        return len(self.header)

    def rowCount(self, parent=QModelIndex()):
        return len(self.get_items())

    def headerData(self, column, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[column]
        return None

    def data(self, index, role):
        row = index.row()
        column = index.column()
        reminder = self.get_items()[row]
        if role == Qt.UserRole:
            return reminder
        if (index.isValid()) and (role == Qt.DisplayRole):
            if column == 0:
                return str(reminder.get_date())  # .toString("yyyy-MM-dd")
            elif column == 1:
                return reminder.get_horse().get_name()
            elif column == 2:
                return reminder.get_comments()
            elif column == 3:
                return reminder.is_done_str()
            else:
                return None
        else:
            return None

    def add_reminder(self, date, horse, comments, is_done):
        indice = len(self.get_items())
        self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_reminders.add_reminder(
            date, horse.dtb_id, comments, is_done)
        self.endInsertRows()

    def edit_reminder(self, date, horse, comments, is_done, reminder):
        self.dtb.table_reminders.update_reminder(
            reminder.dtb_id, date, horse.dtb_id, comments, is_done)

    def delete_reminder(self, reminder):
        #self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_reminders.delete_reminder(reminder.dtb_id)
        # self.endInsertRows()
        self.layoutChanged.emit()


if __name__ == '__main__':
    pass
    #listmodel = ListModel([obj1,obj2,obj3])
