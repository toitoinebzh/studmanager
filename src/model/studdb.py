#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class to handle relation between UI and stud database(sqlite)
"""

import sqlite3
import os.path

from model.constants import __version__

from model.horsedb import Horse, Breed, Sex, Color, MovementRecord, HealthRecord
from model.reminderdb import Reminder
from model.contactdb import Contact

from PyQt5 import QtCore
translate = QtCore.QCoreApplication.translate


class StudDb():
    "main class to interact with database .sqlite"

    def __init__(self, filename):

        self.file = filename
        print(self.file)

        self.table_horse = TableHorse(self)
        self.table_breed = TableBreed(self)
        self.table_color = TableColor(self)
        self.table_sex = TableSex(self)
        self.table_health_records = TableHealthRecords(self)
        self.table_movement_records = TableMovementRecords(self)
        self.table_reminders = TableReminders(self)
        self.table_contact = TableContact(self)
        self.table_infos = TableInfos(self)

        if os.path.exists(self.file):
            print("File already exists")

            self.conn = sqlite3.connect(self.file)
            self.cur = self.conn.cursor()

        else:
            print("Creating database")
            self.conn = sqlite3.connect(self.file)
            self.cur = self.conn.cursor()

            self.create_new_db()

        self.cur.execute('PRAGMA foreign_keys=ON;')
#        print("\n".join(self.__dict__))
#        print("\n")
#        print("\n".join(self.table_breed.__dict__))
#        print("\n")
#        print("\n".join(self.table_breed.db.__dict__))
# =========================================================================
# create new db
# =========================================================================

    def create_new_db(self):
        "create a new database"

        # infos
        self.table_infos.create_table_infos()
        self.table_infos.set_version(__version__)
        # horse tables
        self.table_horse.create_table_horse()
        self.table_breed.create_table_breed()
        self.table_color.create_table_color()
        self.table_sex.create_table_sex()

        self.table_health_records.create_table_health_records()
        self.table_movement_records.create_table_movement_record()

        # reminders
        self.table_reminders.create_table_reminders()

        # contact table
        self.table_contact.create_table_contact()
        # view
        self.create_view()

        self.init_dtb()

        self.conn.commit()
        print("Database created")

    def create_view(self):
        "create views"
        #  Horse
        self.cur.execute('''
            CREATE VIEW `My Horses` AS
            Select THorse.name, THorse.birthdate, TBreed.name, TColor.name, TSex.name from THorse
            left join TColor on TColor.id=THorse.id_color
            left join TSex on TSex.id=THorse.id_sex
            left join TBreed on TBreed.id=THorse.id_breed
            order by THorse.name;
                        ''')
        self.cur.execute('''
            CREATE VIEW `Horses number` AS
            SELECT COUNT(*) FROM (select THorse.name from THorse)
                        ''')
        self.cur.execute('''
            CREATE VIEW `Horse Details` AS
            select chev.name, chev.birthdate, TSex.name, TBreed.name , TColor.name from THorse as chev
            left join TBreed on chev.id_breed=TBreed.id
            left join TColor on chev.id_color=TColor.id
            left join TSex on chev.id_sex=TSex.id
            order by chev.name
                        ''')
        # contact
        self.cur.execute('''
            CREATE VIEW `Contact number` AS
            SELECT COUNT(*) FROM (select TContact.id from TContact)
                        ''')
        self.cur.execute('''
            CREATE VIEW `Contact Details` AS
            select TContact.last_name, TContact.first_name, TContact.organization, TContact.address, TContact.tel, TContact.email FROM TContact
            order by TContact.last_name
                        ''')

        # health records
        self.cur.execute('''
            CREATE VIEW `Health Records Details` AS
            select THealthRecords.date, TContact.last_name, TContact.first_name, TContact.organization, THorse.name, THealthRecords.comments FROM THealthRecords
            left join TContact on TContact.id=THealthRecords.id_contact
            left join THorse on THorse.id=THealthRecords.id_horse
            order by THealthRecords.date
                        ''')

        # movement records
        self.cur.execute('''
            CREATE VIEW `Movement Records Details` AS
            select TMovementRecords.date, THorse.name, TMovementRecords.comments FROM TMovementRecords
            left join THorse on THorse.id=TMovementRecords.id_horse
            order by TMovementRecords.date
                        ''')
        # reminders records
        self.cur.execute('''
            CREATE VIEW `Reminders Details` AS
            select TReminders.date, THorse.name, TReminders.comments, TReminders.is_done FROM TReminders
            left join THorse on THorse.id=TReminders.id_horse
            order by TReminders.date
                        ''')

        # self.conn.commit()

    def init_dtb(self):
        "add basic data (breed,sex,color) into the database"

        self.table_breed.init_dtb()
        self.table_sex.init_dtb()
        self.table_color.init_dtb()

    def save(self):
        "save database"
        self.conn.commit()

    def close(self):
        "close database"
        self.conn.close()


class TableHealthRecords():
    "class to interact with THealthRecords"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_health_records(self):
        "create THealthRecords"
        self.dtb.cur.execute('''
                CREATE TABLE "THealthRecords" (
                	"id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                	"date"	DATE,
                	"id_contact"	INTEGER,
                	"id_horse"	INTEGER,
                	"comments"	TEXT,
                	FOREIGN KEY("id_contact") REFERENCES "TContact"("id"),
                    FOREIGN KEY("id_horse") REFERENCES "THorse"("id") ON DELETE CASCADE
                );
                        ''')
        # self.conn.commit()

    # health records

    def add_health_record(self, date, id_contact, id_horse, comments):
        "add health record"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO THealthRecords (date,id_contact,id_horse,comments)
                                VALUES ( ?,?,?,? )''', (
                date,
                id_contact,
                id_horse,
                comments,
            ))
        id_health_record = self.dtb.cur.lastrowid
        print("Health record created")
        print(HealthRecord(self.dtb, id_health_record))
        # self.conn.commit()

    def update_health_record(
            self,
            id_health_record,
            date,
            id_contact,
            id_horse,
            comments):
        "update health record"
        self.dtb.cur.execute('''UPDATE THealthRecords
                             SET (date,id_contact,id_horse,comments) = (?,?,?,? )
                             WHERE THealthRecords.id=? ''', ((
            date,
            id_contact,
            id_horse,
            comments,
            id_health_record,
        )))
        print("Health record updated")
        print(HealthRecord(self.dtb, id_health_record))

    def delete_health_record(self, id_hr):
        "delete health record"
        self.dtb.cur.execute('''DELETE FROM THealthRecords WHERE id = ? ''',
                             ((id_hr, )))


class TableMovementRecords():
    "class to interact with TMovementRecords"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_movement_record(self):
        "create TMovementRecords"
        self.dtb.cur.execute('''
            CREATE TABLE `TMovementRecords` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_horse` INTEGER,
              `comments`    TEXT,
              FOREIGN KEY("id_horse") REFERENCES "THorse"("id") ON DELETE CASCADE
            );
                        ''')
        # self.conn.commit()

    # movement records

    def add_movement_record(self, date, id_horse, comments):
        "add movement record"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO TMovementRecords (date,id_horse,comments)
                VALUES ( ?,?,? )''', (
                date,
                id_horse,
                comments,
            ))
        id_movement_record = self.dtb.cur.lastrowid
        print("Movement record created")
        print(MovementRecord(self.dtb, id_movement_record))
        # self.conn.commit()

    def update_movement_record(
            self,
            id_movement_record,
            date,
            id_horse,
            comments):
        "update movement record"
        self.dtb.cur.execute('''UPDATE TMovementRecords
                             SET (date,id_horse,comments) = (?,?,? )
                             WHERE TMovementRecords.id=?  ''', ((
            date,
            id_horse,
            comments,
            id_movement_record,
        )))
        print("Movement updated")
        print(MovementRecord(self.dtb, id_movement_record))

    def delete_movement_record(self, id_movement_record):
        "delete movement"
        self.dtb.cur.execute('''DELETE FROM TMovementRecords WHERE id = ? ''',
                             ((id_movement_record, )))


class TableColor():
    "class to interact with TSex"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data into TColor"
        # color
        data = [translate("TableColor", 'Bay'),
                translate("TableColor", 'Bay Overo'),
                translate("TableColor", 'Bay Roan'),
                translate("TableColor", 'Black'),
                translate("TableColor", 'Black Overo'),
                translate("TableColor", 'Blue Grulla'),
                translate("TableColor", 'Blue Roan'),
                translate("TableColor", 'Brindle'),
                translate("TableColor", 'Brown'),
                translate("TableColor", 'Buckskin'),
                translate("TableColor", 'Buckskin Overo'),
                translate("TableColor", 'Champagne'),
                translate("TableColor", 'Chestnut'),
                translate("TableColor", 'Chestnut Overo'),
                translate("TableColor", 'Chocolate'),
                translate("TableColor", 'Cremello'),
                translate("TableColor", 'Dun'),
                translate("TableColor", 'Dun w/ Blk Points'),
                translate("TableColor", 'Dunalino'),
                translate("TableColor", 'Dunskin'),
                translate("TableColor", 'Grey'),
                translate("TableColor", 'Grulla'),
                translate("TableColor", 'Liver Chestnut'),
                translate("TableColor", 'Overo'),
                translate("TableColor", 'Palomino'),
                translate("TableColor", 'Perlino'),
                translate("TableColor", 'Piebald'),
                translate("TableColor", 'Pinto'),
                translate("TableColor", 'Red Dun'),
                translate("TableColor", 'Red Roan'),
                translate("TableColor", 'Roan'),
                translate("TableColor", 'Sabino'),
                translate("TableColor", 'Smokey Black'),
                translate("TableColor", 'Sorrel'),
                translate("TableColor", 'Sorrel Overo'),
                translate("TableColor", 'Tobiano'),
                translate("TableColor", 'Tovero'),
                translate("TableColor", 'White')]

        for color in data:
            self.add_color(color)

    def create_table_color(self):
        "create TColor"
        self.dtb.cur.execute('''
            CREATE TABLE `TColor` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_color(self, color):
        "add color"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TColor (name)
                VALUES ( ? )''', (color, ))
        # self.conn.commit()

    # =========================================================================
    # colors informations
    # =========================================================================

    def _get_list_id_colors(self):
        "return colors id"
        self.dtb.cur.execute(
            "select TColor.id From TColor ORDER BY TColor.name")
        list_id_colors = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_colors]

    def get_colors(self):
        "return colors"
        list_id_colors = self._get_list_id_colors()
        return [Color(self.dtb, i) for i in list_id_colors]


class TableInfos():
    "class to interact with TInfos"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_infos(self):
        "create TInfos"
        self.dtb.cur.execute('''
            CREATE TABLE "TInfos" (
            	"id"	INTEGER CHECK(id=0),
            	"id_manager"	INTEGER,
            	"version"	TEXT,
            	PRIMARY KEY("id"),
            	FOREIGN KEY("id_manager") REFERENCES "TContact"("id")
            );
                        ''')
        self.dtb.cur.execute('''INSERT INTO TInfos (id, version)
                VALUES (?, ? )''', (0, "", ))
        # self.conn.commit()

    # =========================================================================
    # infos
    # =========================================================================

    def set_manager(self, manager):
        "set manager id"
        # self.cur.execute('''UPDATE TInfos SET (id_manager)
        # = (?) WHERE TInfos.id=0  ''', ( (id_manager, )))
        id_manager = manager.dtb_id
        self.dtb.cur.execute('''UPDATE TInfos SET (id_manager)=?
                    WHERE TInfos.id=? ''', ((
            id_manager,
            0,
        )))
        print("New Manager id : ", id_manager)
        print(manager)

    def get_manager(self):
        "return manager"
        self.dtb.cur.execute(
            'SELECT id_manager FROM TInfos WHERE TInfos.id=0 ')
        id_manager = self.dtb.cur.fetchone()[0]
        return Contact(self.dtb, id_manager)

    def set_version(self, version):
        "set version"
        self.dtb.cur.execute('''UPDATE TInfos SET (version)=?
                    WHERE TInfos.id=? ''', ((
            version,
            0,
        )))

    def get_version(self):
        "return manager"
        self.dtb.cur.execute(
            'SELECT version FROM TInfos WHERE TInfos.id=0 ')
        version = self.dtb.cur.fetchone()[0]
        return version


class TableSex():
    "class to interact with TSex"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data int TSex"
        # sex
        data = [translate("TableSex", 'Female'),
                translate("TableSex", 'Male'),
                translate("TableSex", 'Gelding')]
        for sex in data:
            self.add_sex(sex)

    def create_table_sex(self):
        "create TSex"
        self.dtb.cur.execute('''
            CREATE TABLE `TSex` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_sex(self, sex):
        "add sex"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TSex (name)
                VALUES ( ? )''', (sex, ))
        # self.conn.commit()

    # =========================================================================
    # sex informations
    # =========================================================================

    def _get_list_id_sexes(self):
        "return sexes id"
        self.dtb.cur.execute("select TSex.id From TSex ORDER BY TSex.name")
        list_id_sexes = self.dtb.cur.fetchall()
        # self.dtb.conn.commit()
        return [item[0] for item in list_id_sexes]

    def get_sexes(self):
        "return sexes"
        list_id_sexes = self._get_list_id_sexes()
        return [Sex(self.dtb, i) for i in list_id_sexes]


class TableBreed():
    "class to interact with TBreed"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data init TBreed"
        # breed
        data = [translate("TableBreed", 'Akhal Teke'),
                translate("TableBreed", 'American Cream'),
                translate("TableBreed", 'Andalusian'),
                translate("TableBreed", 'Anglo-Arabian'),
                translate("TableBreed", 'Appaloosa'),
                translate("TableBreed", 'Appendix'),
                translate("TableBreed", 'AraAppaloosa'),
                translate("TableBreed", 'Arabian'),
                translate("TableBreed", 'Araloosa'),
                translate("TableBreed", 'Australian Stock'),
                translate("TableBreed", 'Azteca'),
                translate("TableBreed", 'Barb'),
                translate("TableBreed", 'Bashkir Curly'),
                translate("TableBreed", 'Belgian'),
                translate("TableBreed", 'Belgian Warmblood'),
                translate("TableBreed", 'Budyonny'),
                translate("TableBreed", 'Canadian'),
                translate("TableBreed", 'Chincoteague Pony'),
                translate("TableBreed", 'Cleveland Bay'),
                translate("TableBreed", 'Clydesdale'),
                translate("TableBreed", 'Connemara Pony'),
                translate("TableBreed", 'Crossbred Pony'),
                translate("TableBreed", 'Curly'),
                translate("TableBreed", 'Dales Pony'),
                translate("TableBreed", 'Dartmoor Pony'),
                translate("TableBreed", 'Donkey'),
                translate("TableBreed", 'Draft'),
                translate("TableBreed", 'Drum'),
                translate("TableBreed", 'Exmoor Pony'),
                translate("TableBreed", 'Dutch Warmblood'),
                translate("TableBreed", 'Fell Pony'),
                translate("TableBreed", 'Fjord'),
                translate("TableBreed", 'Florida Cracker'),
                translate("TableBreed", 'Friesian'),
                translate("TableBreed", 'Gotland Pony'),
                translate("TableBreed", 'Gypsy Vanner'),
                translate("TableBreed", 'Hackney'),
                translate("TableBreed", 'Haflinger'),
                translate("TableBreed", 'Half Arabian'),
                translate("TableBreed", 'Hanoverian'),
                translate("TableBreed", 'Harlequin'),
                translate("TableBreed", 'Highland Pony'),
                translate("TableBreed", 'Holsteiner'),
                translate("TableBreed", 'Hungarian'),
                translate("TableBreed", 'Iberian'),
                translate("TableBreed", 'Icelandic'),
                translate("TableBreed", 'Irish Draught'),
                translate("TableBreed", 'Kentucky Mountain'),
                translate("TableBreed", 'Knabstrupper'),
                translate("TableBreed", 'Lipizzan'),
                translate("TableBreed", 'Lusitano'),
                translate("TableBreed", 'Marchador'),
                translate("TableBreed", 'Miniature'),
                translate("TableBreed", 'Missouri Fox Trotter'),
                translate("TableBreed", 'Morab'),
                translate("TableBreed", 'Morgan'),
                translate("TableBreed", 'Mountain Pleasure'),
                translate("TableBreed", 'Mule'),
                translate("TableBreed", 'Mustang'),
                translate("TableBreed", 'National Show'),
                translate("TableBreed", 'Natl Sport Perf. Pony'),
                translate("TableBreed", 'New Forest Pony'),
                translate("TableBreed", 'Newfoundland Pony'),
                translate("TableBreed", 'Nokota'),
                translate("TableBreed", 'Oldenburg'),
                translate("TableBreed", 'Paint'),
                translate("TableBreed", 'Paint Pony'),
                translate("TableBreed", 'Palomino'),
                translate("TableBreed", 'Paso Fino'),
                translate("TableBreed", 'Percheron'),
                translate("TableBreed", 'Peruvian Paso'),
                translate("TableBreed", 'Pintabian'),
                translate("TableBreed", 'Pinto'),
                translate("TableBreed", 'Pony'),
                translate("TableBreed", 'POA'),
                translate("TableBreed", 'Quarab'),
                translate("TableBreed", 'Quarter Horse'),
                translate("TableBreed", 'Quarter Pony'),
                translate("TableBreed", 'Racking'),
                translate("TableBreed", 'Rocky Mountain'),
                translate("TableBreed", 'Saddlebred'),
                translate("TableBreed", 'Selle Francais'),
                translate("TableBreed", 'Shagya'),
                translate("TableBreed", 'Shetland Pony'),
                translate("TableBreed", 'Shire'),
                translate("TableBreed", 'Single Footing'),
                translate("TableBreed", 'Spanish Jennet'),
                translate("TableBreed", 'Spanish Mustang'),
                translate("TableBreed", 'Spotted'),
                translate("TableBreed", 'Spotted Saddle'),
                translate("TableBreed", 'Standardbred'),
                translate("TableBreed", 'Tennessee Walking'),
                translate("TableBreed", 'Thoroughbred'),
                translate("TableBreed", 'Tiger'),
                translate("TableBreed", 'Trakehner'),
                translate("TableBreed", 'Warmblood'),
                translate("TableBreed", 'Walkaloosa'),
                translate("TableBreed", 'Welsh Cob'),
                translate("TableBreed", 'Welsh Pony'),
                translate("TableBreed", 'Westphalian'),
                translate("TableBreed", 'Ardennais'),
                translate("TableBreed", 'AQPS'),
                translate("TableBreed", 'Auvergne horse'),
                translate("TableBreed", 'Auxois'),
                translate("TableBreed", 'Boulonnais horse'),
                translate("TableBreed", 'Camargue horse'),
                translate("TableBreed", 'Castillonnais'),
                translate("TableBreed", 'Comtois horse'),
                translate("TableBreed", 'Corsican horse'),
                translate("TableBreed", 'French Saddle Pony'),
                translate("TableBreed", 'French Trotter'),
                translate("TableBreed", 'Henson horse'),
                translate("TableBreed", 'Landais pony'),
                translate("TableBreed", 'Merens horse'),
                translate("TableBreed", 'Nivernais horse'),
                translate("TableBreed", 'Norman Cob'),
                translate("TableBreed", 'Poitevin horse'),
                translate("TableBreed", 'Pottok')]

        for breed in data:
            self.add_breed(breed)

    def create_table_breed(self):
        "create TBreed"
        self.dtb.cur.execute('''
            CREATE TABLE `TBreed` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_breed(self, breed):
        "add breed"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TBreed (name)
                VALUES ( ? )''', (breed, ))
        # self.conn.commit()

    # =========================================================================
    # breeds informations
    # =========================================================================

    def _get_list_id_breeds(self):
        "return breed id"
        self.dtb.cur.execute(
            "select TBreed.id From TBreed ORDER BY TBreed.name")
        list_id_breeds = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_breeds]

    def get_breeds(self):
        "return breeds"
        list_id_breeds = self._get_list_id_breeds()
        return [Breed(self.dtb, i) for i in list_id_breeds]


class TableReminders():
    "class to interact with TReminders"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_reminders(self):
        "create TReminders"
        self.dtb.cur.execute('''
            CREATE TABLE `TReminders` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_horse` INTEGER,
              `comments`    TEXT,
              `is_done`         INTEGER,
              FOREIGN KEY("id_horse") REFERENCES "THorse"("id") ON DELETE CASCADE
            );
                        ''')
        # self.conn.commit()

    # =========================================================================
    # reminders
    # =========================================================================
    # reminders

    def add_reminder(self, date, id_horse, comments, is_done):
        "add reminder"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO TReminders (date,id_horse,comments,is_done)
                VALUES ( ?,?,?,? )''', (
                date,
                id_horse,
                comments,
                int(is_done),
            ))
        id_reminder = self.dtb.cur.lastrowid
        print("Reminder record created")
        print(Reminder(self.dtb, id_reminder))
        # self.conn.commit()

    def update_reminder(self, id_reminder, date, id_horse, comments, is_done):
        "update reminder"
        self.dtb.cur.execute('''UPDATE TReminders
                             SET (date,id_horse,comments,is_done) = (?,?,?,? )
                             WHERE TReminders.id=?  ''', ((
            date,
            id_horse,
            comments,
            int(is_done),
            id_reminder,
        )))
        print("Reminder record updated")
        print(Reminder(self.dtb, id_reminder))

    def delete_reminder(self, id_reminder):
        "delete reminder"
        self.dtb.cur.execute('''DELETE FROM TReminders WHERE id = ? ''',
                             ((id_reminder, )))

    # =========================================================================
    # reminder informations
    # =========================================================================

    def get_reminders(self):
        "return all reminders"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_reminders_done(self):
        "return reminders done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=1
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_reminders_not_done(self):
        "return reminders not done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=0
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_nb_reminders_not_done(self):
        "return number of reminders not done"
        return len(self.get_reminders_not_done())

    def get_reminders_late_not_done(self):
        "retunr reminders late and not done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=0 and TReminders.date<=date('now')
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_nb_reminders_late_not_done(self):
        "return number of reminders late and not done"
        return len(self.get_reminders_late_not_done())


class TableHorse():
    "class to interact with THorse"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_horse(self):
        "create THorse"
        self.dtb.cur.execute('''
            CREATE TABLE `THorse` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE,
                `id_breed`    INTEGER,
                `id_color`    INTEGER,
                `id_sex`    INTEGER,
                `birthdate`    DATE,
                FOREIGN KEY("id_breed") REFERENCES "TBreed"("id"),
                FOREIGN KEY("id_color") REFERENCES "TColor"("id"),
                FOREIGN KEY("id_sex") REFERENCES "TSex"("id")
            );
                        ''')
        # self.conn.commit()

    def add_horse(self, name, id_sex, id_color, birthdate, id_breed):
        "add horse"
        if name == "":
            print("Name is empty !")
            return None
        else:
            self.dtb.cur.execute('''INSERT OR IGNORE INTO THorse
                                 (name,id_breed, id_color,id_sex,birthdate)
                                 VALUES (?,?,?,?,? )''', ((
                name,
                id_breed,
                id_color,
                id_sex,
                birthdate,
            )))

            # self.conn.commit()
            id_horse = self.dtb.cur.lastrowid
            print("New horse created")
            print(Horse(self.dtb, id_horse))
            return id_horse

    def update_horse(self, id_horse, name, id_sex, id_color, birthdate,
                     id_breed):
        "update horse infos"
        if name == "":
            print("Name is empty !")
        else:
            self.dtb.cur.execute(
                '''UPDATE THorse
                                 SET (name,id_breed, id_color,id_sex,birthdate)
                                 = (?,?,?,?,? )
                                 WHERE THorse.id=? ''',
                ((name, id_breed, id_color, id_sex, birthdate, id_horse)))
            print("Horse updated")
            print(Horse(self.dtb, id_horse))
            # self.conn.commit()

    def remove_horse(self, id_horse):
        "remove horse from database"
        self.dtb.cur.execute('''DELETE FROM THorse WHERE id = ? ''',
                             ((id_horse, )))
    # =========================================================================
    # horses informations
    # =========================================================================

    def _get_list_id_horses(self):
        "return horses id"
        self.dtb.cur.execute(
            "select THorse.id From THorse ORDER BY THorse.name")
        list_id_horses = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_horses]

    def get_horses(self):
        "return horses"
        list_id_horses = self._get_list_id_horses()
        return [Horse(self.dtb, i) for i in list_id_horses]

    def get_nb_horses(self):
        "return number of horses"
        return len(self.get_horses())

    def get_horses_in(self):
        "return horses in the stud"
        list_horses_in = []
        for horse in self.get_horses():
            if bool(horse.is_in()):
                list_horses_in.append(horse)
        return list_horses_in

    def get_nb_horses_in(self):
        "return number of horses in the stud"
        return len(self.get_horses_in())


class TableContact():
    "class to interact with TContact"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_contact(self):
        "create TContact"
        self.dtb.cur.execute('''
            CREATE TABLE `TContact` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `last_name`    TEXT,
                `first_name`    TEXT,
                `address`    TEXT,
                `tel`            TEXT,
                `email`     TEXT,
                `organization`    TEXT,
                UNIQUE (last_name,first_name)

            );
                        ''')
        # self.conn.commit()

    # contact
    def add_contact(
            self,
            last_name,
            first_name,
            address,
            tel,
            email,
            organization):
        "add contact"
        if last_name == "" or first_name == "":
            print("Name and first name are empty !")
            return None
        else:
            self.dtb.cur.execute('''INSERT OR IGNORE INTO
                                 TContact (last_name, first_name, address, tel,
                                 email, organization)
                                 VALUES (?,?,?,?,?,? )''', ((
                last_name,
                first_name,
                address,
                tel,
                email,
                organization,
            )))

            # self.conn.commit()
            id_contact = self.dtb.cur.lastrowid
            print("New contact created")
            print(Contact(self.dtb, id_contact))
            return id_contact

    def update_contact(self, id_contact, last_name, first_name, address, tel,
                       email, organization):
        "update contact infos"
        if last_name == "" or first_name == "":
            print("Name and first name are empty !")
        else:
            self.dtb.cur.execute('''UPDATE TContact
                SET (last_name,first_name, address,tel,email, organization)
                    = (?,?,?,?,?,? ) WHERE TContact.id=? ''', ((
                last_name,
                first_name,
                address,
                tel,
                email,
                organization,
                id_contact,
            )))

            print("Contact updated")
            print(Contact(self.dtb, id_contact))

        # self.conn.commit()

    def remove_contact(self, id_contact):
        "remove contact from database"
        self.dtb.cur.execute('''DELETE FROM TContact WHERE id = ? ''',
                             ((id_contact, )))
    # =========================================================================
    # contact informations
    # =========================================================================

    def _get_list_id_contacts(self):
        "return contacts id"
        self.dtb.cur.execute(
            "select TContact.id From TContact ORDER BY TContact.last_name")
        list_id_contacts = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_contacts]

    def get_contacts(self):
        "return contacts"
        list_id_contacts = self._get_list_id_contacts()
        return [Contact(self.dtb, i) for i in list_id_contacts]

    def get_nb_contacts(self):
        "return number of contacts"
        return len(self.get_contacts())
