#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 18:53:36 2020

@author: antoine
"""

from PyQt5.QtCore import QModelIndex
import random

from model.list_model import ListModel
from model.contactdb import Contact


class ContactModel(ListModel):

    def __init__(self, studb, parent=None):
        self.dtb = studb
        ListModel.__init__(
            self,
            self.dtb,
            self.dtb.table_contact.get_contacts,
            parent)

    def add_contact(self):
        #indice = len(self.get_items())
        indice = random.randint(0, 1000)

        last_name = self.tr("New contact")
        first_name = self.tr("John Doe {}").format(str(indice))
        address = ""
        tel = ""
        email = ""
        organization = ""

        self.beginInsertRows(QModelIndex(), indice, indice)
        id_contact = self.dtb.table_contact.add_contact(
            last_name, first_name, address, tel, email, organization)
        self.endInsertRows()

        return Contact(self.dtb, id_contact)

    def remove_contact(self, contact):
        print(contact)
        #indice = contact.dtb_id
        # self.beginRemoveRows(QModelIndex(),indice)
        print("suppression")
        self.dtb.table_contact.remove_contact(contact.dtb_id)
        # self.endRemoveRows()
        self.layoutChanged.emit()
