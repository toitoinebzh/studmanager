#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 14:09:40 2020

@author: antoine
"""

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# https://www.learnpyqt.com/courses/model-views/modelview-architecture/

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt


class TableHealthRecordModel(QAbstractTableModel):

    def __init__(self, horse, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.horse = horse
        self.dtb = horse.dtb
        self.items = horse.get_health_records

        self.header = [
            self.tr("Date"),
            self.tr("Contact"),
            self.tr("Comments")]

    def get_items(self):
        return self.items()

    def columnCount(self, parent=QModelIndex()):
        return len(self.header)

    def rowCount(self, parent=QModelIndex()):
        return len(self.get_items())

    def headerData(self, column, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[column]
        return None

    def data(self, index, role):
        row = index.row()
        column = index.column()
        health_record = self.get_items()[row]
        if role == Qt.UserRole:
            return health_record
        if (index.isValid()) and (role == Qt.DisplayRole):
            if column == 0:
                return str(health_record.get_date())  # .toString("yyyy-MM-dd")
            elif column == 1:
                return health_record.get_contact().get_name()
            elif column == 2:
                return health_record.get_comments()
            else:
                return None
        else:
            return None

    def add_health_record(self, date, contact, horse, comments):
        indice = len(self.get_items())
        self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_health_records.add_health_record(
            date, contact.dtb_id, horse.dtb_id, comments)
        self.endInsertRows()

    def edit_health_record(
            self,
            date,
            contact,
            horse,
            comments,
            health_record):
        self.dtb.table_health_records.update_health_record(
            health_record.dtb_id, date, contact.dtb_id, horse.dtb_id, comments)

    def delete_health_record(self, health_record):
        #self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_health_records.delete_health_record(
            health_record.dtb_id)
        # self.endInsertRows()
        self.layoutChanged.emit()


if __name__ == '__main__':
    pass
    #listmodel = ListModel([obj1,obj2,obj3])
