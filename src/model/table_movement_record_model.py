#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 14:09:40 2020

@author: antoine
"""

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# https://www.learnpyqt.com/courses/model-views/modelview-architecture/
from PyQt5 import QtCore
from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
translate = QtCore.QCoreApplication.translate


def event_type(horse, row):
    "return if the event is IN or OUT"
    if horse.is_in():
        in_out_int = 0
    else:
        in_out_int = 1

    if row % 2 == in_out_int:
        event = translate("event type", "In")
    else:
        event = translate("event type", "Out")

    return event


class TableMovementRecordModel(QAbstractTableModel):

    def __init__(self, horse, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.horse = horse
        self.dtb = horse.dtb
        self.items = horse.get_movement_records

        self.header = [self.tr("Date"), self.tr("In/Out"), self.tr("Comments")]

    def get_items(self):
        return self.items()

    def columnCount(self, parent=QModelIndex()):
        return len(self.header)

    def rowCount(self, parent=QModelIndex()):
        return len(self.get_items())

    def headerData(self, column, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[column]
        return None

    def data(self, index, role):
        row = index.row()
        column = index.column()
        movement_record = self.get_items()[row]
        if role == Qt.UserRole:
            return movement_record
        if (index.isValid()) and (role == Qt.DisplayRole):
            if column == 0:
                # .toString("yyyy-MM-dd")
                return str(movement_record.get_date())
            elif column == 1:
                return event_type(self.horse, row)
            elif column == 2:
                return movement_record.get_comments()
            else:
                return None
        else:
            return None

    def add_movement_record(self, date, horse, comments):
        indice = len(self.get_items())
        self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_movement_records.add_movement_record(
            date, self.horse.dtb_id, comments)
        self.endInsertRows()

    def edit_movement_record(self, date, horse, comments, movement_record):
        self.dtb.table_movement_records.update_movement_record(
            movement_record.dtb_id, date, horse.dtb_id, comments)

    def delete_movement_record(self, movement_record):
        #self.beginInsertRows(QModelIndex(), indice, indice)
        self.dtb.table_movement_records.delete_movement_record(
            movement_record.dtb_id)
        # self.endInsertRows()
        self.layoutChanged.emit()


if __name__ == '__main__':
    pass
    #listmodel = ListModel([obj1,obj2,obj3])
