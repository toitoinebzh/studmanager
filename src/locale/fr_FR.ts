<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>CentraWidget</name>
    <message>
        <location filename="../view/centralwidget.py" line="27"/>
        <source>My Horses</source>
        <translation>Mes Chevaux</translation>
    </message>
    <message>
        <location filename="../view/centralwidget.py" line="28"/>
        <source>Contacts</source>
        <translation>Contacts</translation>
    </message>
    <message>
        <location filename="../view/centralwidget.py" line="30"/>
        <source>Reminders</source>
        <translation>Rappels</translation>
    </message>
</context>
<context>
    <name>ContactModel</name>
    <message>
        <location filename="../model/contact_model.py" line="30"/>
        <source>New contact</source>
        <translation>Nouveau contact</translation>
    </message>
    <message>
        <location filename="../model/contact_model.py" line="31"/>
        <source>John Doe {}</source>
        <translation>Jean Dupont {}</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../controller/contact_list.py" line="50"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../controller/contact_list.py" line="41"/>
        <source>You have to select one item to delete.</source>
        <translation>Vous devez sélectionner une entité à supprimer.</translation>
    </message>
    <message>
        <location filename="../controller/contact_list.py" line="50"/>
        <source>This item is related to others operations and can not be deleted.</source>
        <translation>Cette entité est liée à d&apos;autres opérations et ne peut être supprimée.</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../view/health_record_dialog.ui" line="14"/>
        <source>Health Record</source>
        <translation>Soin Vétérinaire</translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="26"/>
        <source>Date:</source>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../view/health_record_dialog.ui" line="40"/>
        <source>Contact:</source>
        <translation>Contact :</translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="50"/>
        <source>Comments:</source>
        <translation>Commentaires :</translation>
    </message>
    <message>
        <location filename="../view/health_record_dialog.ui" line="57"/>
        <source>Treatment: 
Medical prescription number: 
Medicine: </source>
        <translation>Traitement : 
Numéro d&apos;ordonnance : 
Médicaments : </translation>
    </message>
    <message>
        <location filename="../view/movement_record_dialog.ui" line="14"/>
        <source>Movement Record</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <location filename="../view/movement_record_dialog.ui" line="40"/>
        <source>From: 
To: 
Driver: </source>
        <translation>Origine : 
Destination : 
Conducteur : </translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="14"/>
        <source>Reminder Record</source>
        <translation>Rappel</translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="40"/>
        <source>Horse:</source>
        <translation>Cheval :</translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="57"/>
        <source>Add comments...</source>
        <translation>Ajouter un commentaire...</translation>
    </message>
    <message>
        <location filename="../view/reminder_record_dialog.ui" line="74"/>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../view/horse_description_widget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../view/horse_widget.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Name&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Nom&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="20"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../view/horse_widget.ui" line="51"/>
        <source>Health records</source>
        <translation>Soins Vétérinaires</translation>
    </message>
    <message>
        <location filename="../view/horse_widget.ui" line="60"/>
        <source>Movement records</source>
        <translation>Déplacements</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="28"/>
        <source>Last name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="38"/>
        <source>First Name:</source>
        <translation>Prénom :</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="48"/>
        <source>Organization:</source>
        <translation>Organisation :</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="58"/>
        <source>Email:</source>
        <translation>Courriel :</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="68"/>
        <source>Phone :</source>
        <translation>Téléphone :</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="129"/>
        <source>Update</source>
        <translation type="obsolete">Mise à jour</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="26"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="36"/>
        <source>Birthdate:</source>
        <translation>Date de naissance :</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="50"/>
        <source>Color:</source>
        <translation>Robe :</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="64"/>
        <source>Sex:</source>
        <translation>Sexe :</translation>
    </message>
    <message>
        <location filename="../view/horse_description_widget.ui" line="74"/>
        <source>Breed:</source>
        <translation>Race :</translation>
    </message>
    <message>
        <location filename="../view/listview_widget.ui" line="24"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../view/tableview_widget.ui" line="38"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../view/listview_widget.ui" line="38"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../view/contact_description_widget.ui" line="78"/>
        <source>Address:</source>
        <translation>Adresse :</translation>
    </message>
    <message>
        <location filename="../view/listview_widget.ui" line="31"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../view/tableview_widget.ui" line="45"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../view/listview_widget.ui" line="45"/>
        <source>Del</source>
        <translation>Suppr</translation>
    </message>
</context>
<context>
    <name>HorseModel</name>
    <message>
        <location filename="../model/horse_model.py" line="30"/>
        <source>New horse {}</source>
        <translation>Nouveau cheval {}</translation>
    </message>
</context>
<context>
    <name>HorseView</name>
    <message>
        <location filename="../controller/horse_list.py" line="50"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../controller/horse_list.py" line="41"/>
        <source>You have to select one item to delete.</source>
        <translation>Vous devez sélectionner une entité à supprimer.</translation>
    </message>
    <message>
        <location filename="../controller/horse_list.py" line="50"/>
        <source>This item is related to others operations and can not be deleted.</source>
        <translation>Cette entité est liée à d&apos;autres opérations et ne peut être supprimée.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../view/main_window.ui" line="32"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="42"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="55"/>
        <source>toolBar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="76"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="79"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="88"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="91"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="103"/>
        <source>&amp;Save</source>
        <translation>Enregi&amp;strer</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="106"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="115"/>
        <source>&amp;Close</source>
        <translation>F&amp;ermer</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="118"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="127"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="130"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="139"/>
        <source>&amp;About StudManager</source>
        <translation>À propos de StudM&amp;anager</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="142"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="151"/>
        <source>&amp;Report a bug</source>
        <translation>Signale&amp;r un bug</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="173"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="159"/>
        <source>A&amp;bout Qt</source>
        <translation>À propos &amp;de Qt</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="162"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../view/main_window.ui" line="170"/>
        <source>&amp;Website</source>
        <translation>Site &amp;Web</translation>
    </message>
</context>
<context>
    <name>TableBreed</name>
    <message>
        <location filename="../model/studdb.py" line="490"/>
        <source>Akhal Teke</source>
        <translation>Akhal Teke</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="491"/>
        <source>American Cream</source>
        <translation>American Cream</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="492"/>
        <source>Andalusian</source>
        <translation>Andalou</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="493"/>
        <source>Anglo-Arabian</source>
        <translation>Anglo-Arabe</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="494"/>
        <source>Appaloosa</source>
        <translation>Appaloosa</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="495"/>
        <source>Appendix</source>
        <translation>Appendix</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="496"/>
        <source>AraAppaloosa</source>
        <translation>AraAppaloosa</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="497"/>
        <source>Arabian</source>
        <translation>Arabe</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="498"/>
        <source>Araloosa</source>
        <translation>Araloosa</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="499"/>
        <source>Australian Stock</source>
        <translation>Australian Stock</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="500"/>
        <source>Azteca</source>
        <translation>Azteca</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="501"/>
        <source>Barb</source>
        <translation>Barbe</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="502"/>
        <source>Bashkir Curly</source>
        <translation>Bashkir Curly</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="503"/>
        <source>Belgian</source>
        <translation>Trait Belge</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="504"/>
        <source>Belgian Warmblood</source>
        <translation>BWP</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="505"/>
        <source>Budyonny</source>
        <translation>Boudienny</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="506"/>
        <source>Canadian</source>
        <translation>Canadien</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="507"/>
        <source>Chincoteague Pony</source>
        <translation>Chincoteague</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="508"/>
        <source>Cleveland Bay</source>
        <translation>Bai de Cleveland</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="509"/>
        <source>Clydesdale</source>
        <translation>Clydesdale</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="510"/>
        <source>Connemara Pony</source>
        <translation>Poney Connemara</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="511"/>
        <source>Crossbred Pony</source>
        <translation>Crossbred Pony</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="512"/>
        <source>Curly</source>
        <translation>Curly</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="513"/>
        <source>Dales Pony</source>
        <translation>Dales</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="514"/>
        <source>Dartmoor Pony</source>
        <translation>Dartmoor</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="515"/>
        <source>Donkey</source>
        <translation>Ane</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="516"/>
        <source>Draft</source>
        <translation>Draft</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="517"/>
        <source>Drum</source>
        <translation>Drum</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="518"/>
        <source>Exmoor Pony</source>
        <translation>Exmoor</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="519"/>
        <source>Dutch Warmblood</source>
        <translation>KWPN</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="520"/>
        <source>Fell Pony</source>
        <translation>Fell</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="521"/>
        <source>Fjord</source>
        <translation>Fjord</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="522"/>
        <source>Florida Cracker</source>
        <translation>Florida Cracker</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="523"/>
        <source>Friesian</source>
        <translation>Frison</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="524"/>
        <source>Gotland Pony</source>
        <translation>Gotland</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="525"/>
        <source>Gypsy Vanner</source>
        <translation>Cob Gypsy</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="526"/>
        <source>Hackney</source>
        <translation>Hackney</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="527"/>
        <source>Haflinger</source>
        <translation>Haflinger</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="528"/>
        <source>Half Arabian</source>
        <translation>Demi sang Arabe</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="529"/>
        <source>Hanoverian</source>
        <translation>Hanovrien</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="530"/>
        <source>Harlequin</source>
        <translation>Harlequin</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="531"/>
        <source>Highland Pony</source>
        <translation>Highland</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="532"/>
        <source>Holsteiner</source>
        <translation>Holsteiner</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="533"/>
        <source>Hungarian</source>
        <translation>Cheval de sport hongrois</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="534"/>
        <source>Iberian</source>
        <translation>Cheval ibérique</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="535"/>
        <source>Icelandic</source>
        <translation>Islandais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="536"/>
        <source>Irish Draught</source>
        <translation>Trait irlandais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="537"/>
        <source>Kentucky Mountain</source>
        <translation>Kentucky Mountain</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="538"/>
        <source>Knabstrupper</source>
        <translation>Knabstrup</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="539"/>
        <source>Lipizzan</source>
        <translation>Lipizzan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="540"/>
        <source>Lusitano</source>
        <translation>Lusitano</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="541"/>
        <source>Marchador</source>
        <translation>Marchador</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="542"/>
        <source>Miniature</source>
        <translation>Miniature</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="543"/>
        <source>Missouri Fox Trotter</source>
        <translation type="unfinished">Missouri Fox Trotter</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="544"/>
        <source>Morab</source>
        <translation>Morab</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="545"/>
        <source>Morgan</source>
        <translation>Morgan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="546"/>
        <source>Mountain Pleasure</source>
        <translation>Mountain Pleasure</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="547"/>
        <source>Mule</source>
        <translation>Mule</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="548"/>
        <source>Mustang</source>
        <translation>Mustang</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="549"/>
        <source>National Show</source>
        <translation>National Show</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="550"/>
        <source>Natl Sport Perf. Pony</source>
        <translation type="unfinished">Natl Sport Perf. Pony</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="551"/>
        <source>New Forest Pony</source>
        <translation>New Forest</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="552"/>
        <source>Newfoundland Pony</source>
        <translation>Poney de Terre-Neuve</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="553"/>
        <source>Nokota</source>
        <translation>Nokota</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="554"/>
        <source>Oldenburg</source>
        <translation>Oldenbourg</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="555"/>
        <source>Paint</source>
        <translation>Paint Horse</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="556"/>
        <source>Paint Pony</source>
        <translation>Poney Paint</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="557"/>
        <source>Palomino</source>
        <translation>Palomino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="558"/>
        <source>Paso Fino</source>
        <translation>Paso Fino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="559"/>
        <source>Percheron</source>
        <translation>Percheron</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="560"/>
        <source>Peruvian Paso</source>
        <translation>Paso péruvien</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="561"/>
        <source>Pintabian</source>
        <translation>Pintabian</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="562"/>
        <source>Pinto</source>
        <translation>Pinto</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="563"/>
        <source>Pony</source>
        <translation>Poney</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="564"/>
        <source>POA</source>
        <translation>POA</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="565"/>
        <source>Quarab</source>
        <translation>Quarab</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="566"/>
        <source>Quarter Horse</source>
        <translation>Quarter Horse</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="567"/>
        <source>Quarter Pony</source>
        <translation>Quarter Pony</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="568"/>
        <source>Racking</source>
        <translation>Racking horse</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="569"/>
        <source>Rocky Mountain</source>
        <translation>Rocky Mountain</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="570"/>
        <source>Saddlebred</source>
        <translation>Saddlebred</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="571"/>
        <source>Selle Francais</source>
        <translation>Selle Francais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="572"/>
        <source>Shagya</source>
        <translation>Shagya</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="573"/>
        <source>Shetland Pony</source>
        <translation>Poney Shetland</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="574"/>
        <source>Shire</source>
        <translation>Shire</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="575"/>
        <source>Single Footing</source>
        <translation type="unfinished">Single Footing</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="576"/>
        <source>Spanish Jennet</source>
        <translation>Genet d&apos;Espagne</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="577"/>
        <source>Spanish Mustang</source>
        <translation>Mustang espagnol</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="578"/>
        <source>Spotted</source>
        <translation type="unfinished">Spotted</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="579"/>
        <source>Spotted Saddle</source>
        <translation>Spotted Saddle</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="580"/>
        <source>Standardbred</source>
        <translation>Trotteur américain</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="581"/>
        <source>Tennessee Walking</source>
        <translation>Tennessee Walker</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="582"/>
        <source>Thoroughbred</source>
        <translation>Pur Sang</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="583"/>
        <source>Tiger</source>
        <translation type="unfinished">Tiger</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="584"/>
        <source>Trakehner</source>
        <translation>Trakehner</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="585"/>
        <source>Warmblood</source>
        <translation type="unfinished">Warmblood</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="586"/>
        <source>Walkaloosa</source>
        <translation>Walkaloosa</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="587"/>
        <source>Welsh Cob</source>
        <translation>Welsh Cob</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="588"/>
        <source>Welsh Pony</source>
        <translation>Poney Welsh</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="589"/>
        <source>Westphalian</source>
        <translation>Westphalien</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="590"/>
        <source>Ardennais</source>
        <translation>Ardennais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="591"/>
        <source>AQPS</source>
        <translation>AQPS</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="592"/>
        <source>Auvergne horse</source>
        <translation>Cheval d&apos;Auvergne</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="593"/>
        <source>Auxois</source>
        <translation>Auxois</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="594"/>
        <source>Boulonnais horse</source>
        <translation>Boulonnais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="595"/>
        <source>Camargue horse</source>
        <translation>Camargue</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="596"/>
        <source>Castillonnais</source>
        <translation>Castillonnais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="597"/>
        <source>Comtois horse</source>
        <translation>Comtois</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="598"/>
        <source>Corsican horse</source>
        <translation>Cheval Corse</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="599"/>
        <source>French Saddle Pony</source>
        <translation>Poney français de selle</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="600"/>
        <source>French Trotter</source>
        <translation>Trotteur français</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="601"/>
        <source>Henson horse</source>
        <translation>Cheval de Henson</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="602"/>
        <source>Landais pony</source>
        <translation>Landais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="604"/>
        <source>Nivernais horse</source>
        <translation>Nivernais</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="605"/>
        <source>Norman Cob</source>
        <translation>Cob normand</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="606"/>
        <source>Poitevin horse</source>
        <translation>Poitevin</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="607"/>
        <source>Pottok</source>
        <translation>Pottok</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="603"/>
        <source>Merens horse</source>
        <translation>Mérens</translation>
    </message>
</context>
<context>
    <name>TableColor</name>
    <message>
        <location filename="../model/studdb.py" line="297"/>
        <source>Bay</source>
        <translation>Bai</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="298"/>
        <source>Bay Overo</source>
        <translation>Bai Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="299"/>
        <source>Bay Roan</source>
        <translation type="unfinished">Bay Roan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="300"/>
        <source>Black</source>
        <translation>Noir</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="301"/>
        <source>Black Overo</source>
        <translation>Noir Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="302"/>
        <source>Blue Grulla</source>
        <translation type="unfinished">Blue Grulla</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="303"/>
        <source>Blue Roan</source>
        <translation type="unfinished">Blue Roan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="304"/>
        <source>Brindle</source>
        <translation>Bringé</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="305"/>
        <source>Brown</source>
        <translation>Brun</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="306"/>
        <source>Buckskin</source>
        <translation>Buckskin</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="307"/>
        <source>Buckskin Overo</source>
        <translation>Isabelle Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="308"/>
        <source>Champagne</source>
        <translation>Champagne</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="309"/>
        <source>Chestnut</source>
        <translation>Alezan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="310"/>
        <source>Chestnut Overo</source>
        <translation>Alezan Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="311"/>
        <source>Chocolate</source>
        <translation>Chocolat</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="312"/>
        <source>Cremello</source>
        <translation>Cremello</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="313"/>
        <source>Dun</source>
        <translation>Dun</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="314"/>
        <source>Dun w/ Blk Points</source>
        <translation type="unfinished">Dun w/ Blk Points</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="315"/>
        <source>Dunalino</source>
        <translation>Dunalino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="316"/>
        <source>Dunskin</source>
        <translation>Dunskin</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="317"/>
        <source>Grey</source>
        <translation>Gris</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="318"/>
        <source>Grulla</source>
        <translation>Souris</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="319"/>
        <source>Liver Chestnut</source>
        <translation>Liver Chestnut</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="320"/>
        <source>Overo</source>
        <translation>Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="321"/>
        <source>Palomino</source>
        <translation>Palomino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="322"/>
        <source>Perlino</source>
        <translation>Perlino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="323"/>
        <source>Piebald</source>
        <translation>Pie</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="324"/>
        <source>Pinto</source>
        <translation>Pinto</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="325"/>
        <source>Red Dun</source>
        <translation>Alezan Dun</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="326"/>
        <source>Red Roan</source>
        <translation type="unfinished">Red Roan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="327"/>
        <source>Roan</source>
        <translation>Rouan</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="328"/>
        <source>Sabino</source>
        <translation>Sabino</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="329"/>
        <source>Smokey Black</source>
        <translation type="unfinished">Smokey Black</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="330"/>
        <source>Sorrel</source>
        <translation type="unfinished">Sorrel</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="331"/>
        <source>Sorrel Overo</source>
        <translation type="unfinished">Sorrel Overo</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="332"/>
        <source>Tobiano</source>
        <translation>Tobiano</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="333"/>
        <source>Tovero</source>
        <translation>Tovero</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="334"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
</context>
<context>
    <name>TableHealthRecordModel</name>
    <message>
        <location filename="../model/table_health_record_model.py" line="26"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../model/table_health_record_model.py" line="27"/>
        <source>Contact</source>
        <translation>Contact</translation>
    </message>
    <message>
        <location filename="../model/table_health_record_model.py" line="28"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
</context>
<context>
    <name>TableHealthRecordView</name>
    <message>
        <location filename="../controller/table_health_record_view.py" line="98"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../controller/table_health_record_view.py" line="79"/>
        <source>You have to create at least one horse and one contact before adding health record.</source>
        <translation>Vous devez ajouter au moins un cheval et un contact avant d&apos;ajouter un soin vétérinaire.</translation>
    </message>
    <message>
        <location filename="../controller/table_health_record_view.py" line="98"/>
        <source>You have to select one health record to edit.</source>
        <translation>Vous devez sélectionner le soin vétérinaire à modifier.</translation>
    </message>
</context>
<context>
    <name>TableMovementRecordModel</name>
    <message>
        <location filename="../model/table_movement_record_model.py" line="41"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../model/TableMovementRecordModel.py" line="40"/>
        <source>Event</source>
        <translation type="obsolete">Entrée/Sortie</translation>
    </message>
    <message>
        <location filename="../model/table_movement_record_model.py" line="41"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../model/table_movement_record_model.py" line="41"/>
        <source>In/Out</source>
        <translation>Entrée/Sortie</translation>
    </message>
</context>
<context>
    <name>TableMovementRecordView</name>
    <message>
        <location filename="../controller/table_movement_record_view.py" line="117"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../controller/table_movement_record_view.py" line="78"/>
        <source>You have to create at least one horse before adding movement record.</source>
        <translation>Vous devez créer au moins un cheval avant d&apos;ajouter un déplacement.</translation>
    </message>
    <message>
        <location filename="../controller/table_movement_record_view.py" line="97"/>
        <source>You have to select one movement record to edit.</source>
        <translation>Vous devez sélectionner un déplacement à modifier.</translation>
    </message>
    <message>
        <location filename="../controller/table_movement_record_view.py" line="117"/>
        <source>You have to select one movement record to delete.</source>
        <translation>Vous devez sélectionner le mouvement à supprimer.</translation>
    </message>
</context>
<context>
    <name>TableReminderModel</name>
    <message>
        <location filename="../model/table_reminder_model.py" line="25"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../model/table_reminder_model.py" line="26"/>
        <source>Horse</source>
        <translation>Cheval</translation>
    </message>
    <message>
        <location filename="../model/table_reminder_model.py" line="27"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../model/table_reminder_model.py" line="28"/>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
</context>
<context>
    <name>TableReminderView</name>
    <message>
        <location filename="../controller/table_reminder_view.py" line="112"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../controller/table_reminder_view.py" line="73"/>
        <source>You have to create at least one horse before adding reminder record.</source>
        <translation>Vous devez créer au moins un cheval avant d&apos;ajouter un rappel.</translation>
    </message>
    <message>
        <location filename="../controller/table_reminder_view.py" line="92"/>
        <source>You have to select one reminder record to edit.</source>
        <translation>Vous devez sélectionner le rappel à modifier.</translation>
    </message>
    <message>
        <location filename="../controller/table_reminder_view.py" line="112"/>
        <source>You have to select one reminder record to delete.</source>
        <translation>Vous devez sélectionner le rappel à supprimer.</translation>
    </message>
</context>
<context>
    <name>TableSex</name>
    <message>
        <location filename="../model/studdb.py" line="442"/>
        <source>Female</source>
        <translation>Femelle</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="443"/>
        <source>Male</source>
        <translation>Male</translation>
    </message>
    <message>
        <location filename="../model/studdb.py" line="444"/>
        <source>Gelding</source>
        <translation>Hongre</translation>
    </message>
</context>
<context>
    <name>event type</name>
    <message>
        <location filename="../model/table_movement_record_model.py" line="26"/>
        <source>In</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../model/table_movement_record_model.py" line="28"/>
        <source>Out</source>
        <translation>Sortie</translation>
    </message>
</context>
<context>
    <name>mainwindow</name>
    <message>
        <location filename="../controller/mainwindow.py" line="85"/>
        <source>New file</source>
        <translation>Nouveau fichier</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="115"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="132"/>
        <source>File saved successfully</source>
        <translation>Fichier enregistré avec succès</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="138"/>
        <source>No file to save !</source>
        <translation>Pas de fichier à enregistrer !</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="156"/>
        <source>File closed successfully</source>
        <translation>Fichier fermé avec succès</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="162"/>
        <source>No file to close !</source>
        <translation>Pas de fichier à fermer !</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="187"/>
        <source>StudManager {} 
Stud Manager is a software designed to help you to manage your stud.</source>
        <translation>StudManager {} 
Stud Manager est une logiciel conçu pour vous aider à gérer votre haras.</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="191"/>
        <source>This software has been written with Python, PyQt5 and sqlite.</source>
        <translation>Ce logiciel est écrit en Python à l&apos;aide des bibliothèques PyQt5 et sqlite.</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="195"/>
        <source>About StudManager</source>
        <translation>À propos de StudManager</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="216"/>
        <source>Open Url</source>
        <translation>Ouvrir l&apos;url</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="216"/>
        <source>Could not open url</source>
        <translation>Échec lors de l&apos;ouverture de l&apos;url</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="224"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="59"/>
        <source>Save before exit ?</source>
        <translation>Enregistrer avant de quitter ?</translation>
    </message>
    <message>
        <location filename="../controller/mainwindow.py" line="59"/>
        <source>Do you want to save before exit ?</source>
        <translation>Voulez-vous enregistrer avant de quitter ?</translation>
    </message>
</context>
</TS>
