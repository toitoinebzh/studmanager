#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QDate


class InputReminderRecord(QDialog):
    "dialog to create/edit movement records"
#            self.setWindowIcon(QIcon(ICON_PATH + "movements.svg"))

    def __init__(self, studdb, reminder_record=None, parent=None):
        # Call the inherited classes __init__ method
        super(InputReminderRecord, self).__init__(parent)
        uic.loadUi(
            './view/reminder_record_dialog.ui',
            self)  # Load the .ui file

        self.dtb = studdb
        self.comboBox.update(
            self.dtb,
            self.dtb.table_horse.get_horses,
            obj=None)
        self.set_infos(reminder_record)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def set_infos(self, reminder_record):
        # data to display
        if reminder_record is None:
            date = QDate.currentDate()
            comments = None
            horse = None
            is_done = False
        else:
            date = reminder_record.get_date()
            comments = reminder_record.get_comments()
            horse = reminder_record.get_horse()
            is_done = reminder_record.is_done()

        # widget
        self.dateEdit.setDate(date)
        if comments:
            self.plainTextEdit.setPlainText(comments)
        self.comboBox.set_obj(horse)
        self.checkBox.setChecked(is_done)

    def get_infos(self):
        "return infos from dialog"
        date_reminder_record = self.dateEdit.date().toString("yyyy-MM-dd")
        comments = self.plainTextEdit.toPlainText()
        horse = self.comboBox.get_obj()
        is_done = self.checkBox.isChecked()
        return date_reminder_record, horse, comments, is_done


if __name__ == '__main__':
    from reminderdb import Reminder
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    rr = Reminder(dtb, 1)
    widget = InputReminderRecord(dtb, rr)

    if widget.exec_() == QDialog.Accepted:
        print(widget.get_infos())

    # widget.show()
    # sys.exit(app.exec_())
