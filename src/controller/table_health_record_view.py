#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:39:19 2020

@author: antoine
"""
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import QSortFilterProxyModel, Qt

from controller.input_health_record import InputHealthRecord
from model.table_health_record_model import TableHealthRecordModel

# https://stackoverflow.com/questions/5927499/how-to-get-selected-rows-in-qtableview
# setSelectionBehavior(QAbstractItemView::SelectRows);
# setSelectionMode(QAbstractItemView::SingleSelection);


class TableHealthRecordView(QWidget):
    def __init__(self, studb, horse):
        QWidget.__init__(self)
        uic.loadUi('./view/tableview_widget.ui', self)  # Load the .ui file

        self.dtb = studb
        self.horse = horse

        self.set_horse(self.horse)

        self.pushButtonAdd.clicked.connect(self.add_health_record)
        self.pushButtonEdit.clicked.connect(self.edit_health_record)
        self.tableView.doubleClicked.connect(self.edit_health_record)
        self.pushButtonDelete.clicked.connect(self.delete_health_record)

    def set_horse(self, horse):
        self.horse = horse
#        self.list_obj = self.dtb.table_reminders.get_reminders
#        print(self.list_obj)
#        self.list_model = TableReminderModel(self.dtb)
#        self.tableView.setModel(self.list_model)
        if self.horse is not None:
            # self.tableView.setModel(TableHealthRecordModel(self.horse))
            proxymodel = QSortFilterProxyModel()
            self.mymodel = TableHealthRecordModel(self.horse)
            proxymodel.setSourceModel(self.mymodel)
            self.tableView.setModel(proxymodel)
            self.tableView.setSortingEnabled(True)
            self.tableView.sortByColumn(0, Qt.DescendingOrder)

    def get_obj(self):
        try:
            # print(self.tableView.currentIndex().row())
            if self.tableView.currentIndex().row() != -1:
                # return view.model().get_items()[view.currentIndex().row()]
                # return rows[view.currentIndex().row()]
                # return
                # proxymodel.sourceModel().get_items()[view.currentIndex().row()]
                return self.tableView.currentIndex().data(Qt.UserRole)
            return None
        except BaseException:
            return None

    def set_obj(self, obj=None):
        # if (obj is not None) and (obj in self.tableView.model().get_items()):
        if (obj is not None) and (obj in self.mymodel.get_items()):
            position = self.list_obj().index(obj)
            #index = self.tableView.model().index(position)
            index = self.mymodel.index(position)
            self.tableView.setCurrentIndex(index)

    def add_health_record(self):
        "create/edit a reminder record"
        health_record = None

        if (self.dtb.table_horse.get_nb_horses() == 0) or (
                self.dtb.table_contact.get_nb_contacts() == 0):
            QMessageBox.warning(
                self,
                self.tr('Error !'),
                self.tr("You have to create at least one horse and one contact before adding health record."),
                QMessageBox.Ok)
        else:
            input_data = InputHealthRecord(self.dtb, health_record)

            if input_data.exec_() == QDialog.Accepted:
                date_health_record, contact, comments = input_data.get_infos()
                #self.tableView.model().add_health_record(date_health_record, contact, self.horse, comments)
                self.mymodel.add_health_record(
                    date_health_record, contact, self.horse, comments)
            else:
                pass

    def edit_health_record(self):
        health_record = self.get_obj()
        if health_record is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one health record to edit."),
                QMessageBox.Ok)
        else:
            input_data = InputHealthRecord(self.dtb, health_record)

            if input_data.exec_() == QDialog.Accepted:
                date_health_record, contact, comments = input_data.get_infos()

                #self.tableView.model().edit_health_record(date_health_record, contact, self.horse, comments, health_record)
                self.mymodel.edit_health_record(
                    date_health_record, contact, self.horse, comments, health_record)
            else:
                pass

    def delete_health_record(self):
        "delete reminder record"
        health_record = self.get_obj()
        if health_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one reminder record to delete.",
                QMessageBox.Ok)
        else:
            # self.tableView.model().delete_health_record(health_record)
            self.mymodel.delete_health_record(health_record)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb
    from horsedb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    horse = Horse(dtb, 1)
    widget = TableHealthRecordView(dtb, horse)
    widget.show()
    sys.exit(app.exec_())
