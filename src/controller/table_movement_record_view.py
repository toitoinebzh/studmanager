#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:39:19 2020

@author: antoine
"""
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import QSortFilterProxyModel, Qt

from controller.input_movement_record import InputMovementRecord
from model.table_movement_record_model import TableMovementRecordModel

# https://stackoverflow.com/questions/5927499/how-to-get-selected-rows-in-qtableview
# setSelectionBehavior(QAbstractItemView::SelectRows);
# setSelectionMode(QAbstractItemView::SingleSelection);


class TableMovementRecordView(QWidget):
    def __init__(self, studb, horse):
        QWidget.__init__(self)
        uic.loadUi('./view/tableview_widget.ui', self)  # Load the .ui file

        self.dtb = studb
        self.horse = horse

        self.set_horse(self.horse)

        self.pushButtonAdd.clicked.connect(self.add_movement_record)
        self.pushButtonEdit.clicked.connect(self.edit_movement_record)
        self.tableView.doubleClicked.connect(self.edit_movement_record)
        self.pushButtonDelete.clicked.connect(self.delete_movement_record)

    def set_horse(self, horse):
        self.horse = horse
#        self.list_obj = self.dtb.table_reminders.get_reminders
#        print(self.list_obj)
#        self.list_model = TableReminderModel(self.dtb)
#        self.tableView.setModel(self.list_model)
        if self.horse is not None:
            # self.tableView.setModel(TableMovementRecordModel(self.horse))
            proxymodel = QSortFilterProxyModel()
            self.mymodel = TableMovementRecordModel(self.horse)
            proxymodel.setSourceModel(self.mymodel)
            self.tableView.setModel(proxymodel)
            self.tableView.setSortingEnabled(True)
            self.tableView.sortByColumn(0, Qt.DescendingOrder)

    def get_obj(self):
        try:
            # print(self.tableView.currentIndex().row())
            if self.tableView.currentIndex().row() != -1:
                # return view.model().get_items()[view.currentIndex().row()]
                # return rows[view.currentIndex().row()]
                # return
                # proxymodel.sourceModel().get_items()[view.currentIndex().row()]
                return self.tableView.currentIndex().data(Qt.UserRole)
            return None
        except BaseException:
            return None

    def set_obj(self, obj=None):
        # if (obj is not None) and (obj in self.tableView.model().get_items()):
        if (obj is not None) and (obj in self.mymodel.get_items()):
            position = self.list_obj().index(obj)
            #index = self.tableView.model().index(position)
            index = self.mymodel.index(position)
            self.tableView.setCurrentIndex(index)

    def add_movement_record(self):
        "create/edit a reminder record"
        movement_record = None

        if self.dtb.table_horse.get_nb_horses() == 0:
            QMessageBox.warning(
                self,
                self.tr('Error !'),
                self.tr("You have to create at least one horse before adding movement record."),
                QMessageBox.Ok)
        else:
            input_data = InputMovementRecord(self.dtb, movement_record)

            if input_data.exec_() == QDialog.Accepted:
                date_movement_record, comments = input_data.get_infos()
                #self.tableView.model().add_movement_record(date_movement_record, self.horse, comments)
                self.mymodel.add_movement_record(
                    date_movement_record, self.horse, comments)
            else:
                pass

    def edit_movement_record(self):
        movement_record = self.get_obj()
        if movement_record is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one movement record to edit."),
                QMessageBox.Ok)
        else:
            input_data = InputMovementRecord(self.dtb, movement_record)

            if input_data.exec_() == QDialog.Accepted:
                date_movement_record, comments = input_data.get_infos()

                #self.tableView.model().edit_movement_record(date_movement_record, self.horse, comments, movement_record)
                self.mymodel.edit_movement_record(
                    date_movement_record, self.horse, comments, movement_record)
            else:
                pass

    def delete_movement_record(self):
        "delete reminder record"
        movement_record = self.get_obj()
        if movement_record is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one movement record to delete."),
                QMessageBox.Ok)
        else:
            # self.tableView.model().delete_movement_record(movement_record)
            self.mymodel.delete_movement_record(movement_record)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from studdb import StudDb
    from horsedb import Horse

    app = QApplication(sys.argv)
    dtb = StudDb("/home/antoine/Bureau/studmanager/src/sample/demo.sqlite")
    horse = Horse(dtb, 1)
    widget = TableMovementRecordView(dtb, None)
    widget.show()
    sys.exit(app.exec_())
