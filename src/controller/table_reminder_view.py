#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 17:39:19 2020

@author: antoine
"""
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import QSortFilterProxyModel, Qt

from model.table_reminder_model import TableReminderModel
from controller.input_reminder_record import InputReminderRecord

# https://stackoverflow.com/questions/5927499/how-to-get-selected-rows-in-qtableview
# setSelectionBehavior(QAbstractItemView::SelectRows);
# setSelectionMode(QAbstractItemView::SingleSelection);


class TableReminderView(QWidget):
    def __init__(self, studb):
        QWidget.__init__(self)
        uic.loadUi('./view/tableview_widget.ui', self)  # Load the .ui file

        self.dtb = studb

#        self.list_obj = self.dtb.table_reminders.get_reminders
#        print(self.list_obj)
#        self.list_model = TableReminderModel(self.dtb)
#        self.tableView.setModel(self.list_model)

        # self.tableView.setModel(TableReminderModel(self.dtb))
        proxymodel = QSortFilterProxyModel()
        self.mymodel = TableReminderModel(self.dtb)
        proxymodel.setSourceModel(self.mymodel)
        self.tableView.setModel(proxymodel)
        self.tableView.setSortingEnabled(True)
        self.tableView.sortByColumn(0, Qt.AscendingOrder)

        self.pushButtonAdd.clicked.connect(self.add_reminder)
        self.pushButtonEdit.clicked.connect(self.edit_reminder)
        self.tableView.doubleClicked.connect(self.edit_reminder)
        self.pushButtonDelete.clicked.connect(self.delete_reminder)

    def get_obj(self):
        try:
            # print(self.tableView.currentIndex().row())
            if self.tableView.currentIndex().row() != -1:
                # return view.model().get_items()[view.currentIndex().row()]
                # return rows[view.currentIndex().row()]
                # return
                # proxymodel.sourceModel().get_items()[view.currentIndex().row()]
                return self.tableView.currentIndex().data(Qt.UserRole)
            return None
        except BaseException:
            return None

    def set_obj(self, obj=None):
        # if (obj is not None) and (obj in self.tableView.model().get_items()):
        if (obj is not None) and (obj in self.mymodel.get_items()):
            position = self.list_obj().index(obj)
            #index = self.tableView.model().index(position)
            index = self.mymodel.index(position)
            self.tableView.setCurrentIndex(index)

    def add_reminder(self):
        "create/edit a reminder record"
        reminder_record = None

        if self.dtb.table_horse.get_nb_horses() == 0:
            QMessageBox.warning(
                self,
                self.tr('Error !'),
                self.tr("You have to create at least one horse before adding reminder record."),
                QMessageBox.Ok)
        else:
            input_data = InputReminderRecord(self.dtb, reminder_record)

            if input_data.exec_() == QDialog.Accepted:
                date_reminder_record, horse, comments, is_done = input_data.get_infos()
                #self.tableView.model().add_reminder(date_reminder_record, horse, comments, is_done)
                self.mymodel.add_reminder(
                    date_reminder_record, horse, comments, is_done)
            else:
                pass

    def edit_reminder(self):
        reminder_record = self.get_obj()
        if reminder_record is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one reminder record to edit."),
                QMessageBox.Ok)
        else:
            input_data = InputReminderRecord(self.dtb, reminder_record)

            if input_data.exec_() == QDialog.Accepted:
                date_reminder_record, horse, comments, is_done = input_data.get_infos()

                #self.tableView.model().edit_reminder(date_reminder_record, horse, comments, is_done, reminder_record)
                self.mymodel.edit_reminder(
                    date_reminder_record, horse, comments, is_done, reminder_record)
            else:
                pass

    def delete_reminder(self):
        "delete reminder record"
        reminder_record = self.get_obj()
        if reminder_record is None:
            QMessageBox.warning(
                self, self.tr('Error !'),
                self.tr("You have to select one reminder record to delete."),
                QMessageBox.Ok)
        else:
            # self.tableView.model().delete_reminder(reminder_record)
            self.mymodel.delete_reminder(reminder_record)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    from model.studdb import StudDb

    app = QApplication(sys.argv)
    dtb = StudDb("sample/demo.sqlite")
    widget = TableReminderView(dtb)
    widget.show()
    sys.exit(app.exec_())
